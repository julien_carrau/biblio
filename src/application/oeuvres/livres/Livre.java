package application.oeuvres.livres;

import application.oeuvres.Document;

/**
 * Classe héritant de Document, qui représente un livre.
 * Un livre est, en plus d'être potentiellement identifié par un ean, identifié par un isbn.
 * Il sera donc stocké en double dans les HashMap pour le retrouver aussi bien par ean qu'isbn.
 */
public class Livre extends Document {
    private String isbn;

    /**
     * Constructeur de la classe.
     * @param ean Numéro d'EAN unique identifiant le document. @see <a href="https://fr.wikipedia.org/wiki/EAN_13">Page Wikipedia sur l'ean.</a>
     * @param isbn Numéro d'ISBN unique du livre. @see <a href="https://fr.wikipedia.org/wiki/International_Standard_Book_Number">Page Wikipedia sur l'isbn.</a>
     * @param title Titre du document.
     * @param publisher Publicateur du document.
     * @param date Date de publication.
     * @param authorName Nom de l'autheur.
     * @param authorSurname Prénom de l'autheur.
     * @param totalCopies Nombre total de copies disponibles réparties entre les différentes bibliothèques de paris.
     * @see application.oeuvres.Document#Document(String, String, String, String, String, String, int)
     */
    public Livre(String ean, String isbn, String title, String publisher, String date, String authorName, String authorSurname, int totalCopies) {
        super(ean, title, publisher, date, authorName, authorSurname, totalCopies);
        this.isbn = isbn;
    }

    /**
     * Méthode d'accès à l'ISBN du livre.
     * @return L'ISBN du livre.
     */
    public String getIsbn() { return isbn; }

    /**
     * Renvoie une représentation textuelle du livre, identique à celle d'un document mais ajoutant l'ISBN.
     * @return Cette représentation textuelle.
     */
    @Override
    public String toString() {
        return "ISBN : " + getIsbn() + "\n" + super.toString();
    }
}
