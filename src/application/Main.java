package application;

import application.acteurs.Reseau;

/**
 * Programme principal.
 * Il crée un réseau à partir des paramètres d'entrée, ou d'un chemin par défaut s'ils ne sont pas renseignés.
 * La gestion de l'application se fait aussi dans le réseau.
 * @see Reseau
 */
public class Main {
	public static void main(String[] args) {
		Reseau reseau = new Reseau(args);
	}
}
