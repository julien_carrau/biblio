package application.acteurs;

import application.oeuvres.Document;
import application.oeuvres.livres.Livre;
import application.oeuvres.Serie;
import application.oeuvres.livres.types.Autre;
import application.oeuvres.livres.types.BandeDessinee;
import application.oeuvres.livres.types.Carte;
import application.oeuvres.livres.types.Partition;
import application.oeuvres.types.*;
import data.FileReader;
import exceptions.*;

import java.io.File;
import java.security.InvalidParameterException;
import java.text.ParseException;
import java.time.DateTimeException;
import java.util.*;

/**
 * Classe modélisant un réseau de bibliothèques.
 * Les bibliothèques, les documents, les séries et les utilisateurs du réseau sont référencés.
 */
public class Reseau {
    private HashMap<String, Bibliotheque> bibliotheques;
    private HashMap<String, Document> documents;
    private HashMap<String, Livre> livres;
    private HashMap<String, Serie> series;
    private HashMap<Integer, Utilisateur> utilisateurs;

    /**
     * Classe contenant deux objets de type différent.
     * Elle permet qu'une méthode puisse renvoyer deux objets et non un seul.
     * @param <T> Le type du premier objet.
     * @param <U> Le type du deuxième objet.
     */
    private static class Pair<T, U> {
        public T t1;
        public U t2;
        public Pair(T t1, U t2) {
            this.t1 = t1;
            this.t2 = t2;
        }
    }

    /**
     * Constructeur du réseau à partir d'un fichier CSV.
     * @param args Le chemin de ce fichier CSV. Si ce chemin est vide vide, un chemin par défaut sera utilisé.
     */
    public Reseau(String[] args) {
        // Pour ne pas avoir à retaper le file path à chaque fois :
        String csvFilePath;
        if (args.length > 0) csvFilePath = args[0]; // fichier entré en paramètre s'il y en a un
        else csvFilePath = "bin/data/data.csv";	// sinon, fichier par défaut

        File tempFile = new File(csvFilePath);

        if (tempFile.exists()) {
            System.out.println("[Reseau] Reading the file " + csvFilePath + "...");

            FileReader.ResultatGetData lesHashMaps = FileReader.getDataFromCSVFile(csvFilePath);
            this.documents = lesHashMaps.documents;
            this.livres = lesHashMaps.livres;
            this.series = lesHashMaps.series;
            this.bibliotheques = lesHashMaps.bibliotheques;
            this.utilisateurs = new HashMap<>();

            System.out.println("[Reseau] End of the file " + csvFilePath + ".");
        } else {
            System.out.println("[Reseau] No file " + csvFilePath + ".");
        }

        application();
    }

    /**
     * Méthode d'accès à la HashMap de bibliothèques du réseau.
     * @return La HashMap de bibliothèques du réseau.
     */
    public HashMap<String, Bibliotheque> getBibliotheques() { return bibliotheques; }

    /**
     * Méthode d'accès à la HashMap de documents du réseau.
     * @return La HashMap de documents du réseau.
     */
    public HashMap<String, Document> getDocuments() { return documents; }

    /**
     * Méthode d'accès à la HashMap de livres du réseau.
     * @return La HashMap de livres du réseau.
     */
    public HashMap<String, Livre> getLivres() { return livres; }

    /**
     * Méthode d'accès à la HashMap de séries du réseau.
     * @return La HashMap de séries du réseau.
     */
    public HashMap<String, Serie> getSeries() { return series; }

    /**
     * Boucle principale de l'application.
     * Elle est appelée depuis le constructeur du réseau.
     */
    public void application() {
        String help = "Veuillez entrer un numéro pour réaliser l'action correspondante :\n" +
                String.format("%-60s", "[1 nomBibliothèque]")  + "Ajouter une nouvelle bibliothèque dans le réseau.\n" +
                String.format("%-60s", "[2 identifiantDocument]") + "Ajouter un nouveau document dans le réseau par ISBN ou EAN (détection automatique).\n" +
                String.format("%-60s", "[3 nomBibliothèque quotaMaxDocumentsEmpruntables]") + "Ajouter un nouvel utilisateur, associé à une bibliothèque, avec un quota maximum de documents qu’il peut emprunter.\n" +
                String.format("%-60s", "[4 nomBibliothèque(optionnel)]") + "Consulter l’ensemble des documents.\n" +
                String.format("%-60s", "[5 nomDeSérie nomBibliothèque(opt.)]") + "Consulter pour une série, les documents triés par date de publication.\n" +
                String.format("%-60s", "[6 nomAuteur prénomAuteur nomBibliothèque(opt.)]") + "Consulter les documents d’un même auteur (par nom, prénom ou les deux).\n" +
                String.format("%-60s", "[7 isbn nomBibliothèque(opt.)]") + "Rechercher un livre par son ISBN.\n" +
                String.format("%-60s", "[8 ean nomBibliothèque(opt.)]") + "Recherche un document par son EAN.\n" +
                String.format("%-60s", "[9 annéeDébut(opt.) annéeFin(opt.) nomBibliothèque(opt.)]") + "Consulter le nombre de documents de chaque type publiés de annéeDébut à annéeFin (0 à " + Calendar.getInstance().get(Calendar.YEAR) + " par défaut).\n" +
                String.format("%-60s", "[10 idDocument idUtilisateur]") + "L'utilisateur identifié emprunte le document souhaité dans sa bibliothèque.\n" +
                String.format("%-60s", "[11 idDocument idUtilisateur]") + "L'utilisateur identifié rend le document préalablement emprunté dans sa bibliothèque.\n" +
                String.format("%-60s", "[12 idDocument nomBiblioReceveuse nomBiblioDonneuse]") + "La première bibliothèque reçoit le document spécifié de la deuxième.\n" +
                String.format("%-60s", "[-1]") + "Quitter l'application.\n\n" +
                "PS : Pour les actions 4 à 9, pour restreindre la recherche à une bibliothèque en particulier,\n" +
                "ajouter le nom de la bibliothèque en dernier paramètre.\n" +
                "Exemple : 6 Jean Racine OscarWilde : consulter les documents publiés par Jean Racine dans la bibliothèque Oscar Wilde.\n\n" +
                "Voici les noms des bibliothèques connues :\n";
        Pair<Integer, ArrayList<String>> entree; // le numéro de commande est t1 et les paramètres optionnels sont t2
        boolean isOpened = true;

        while(isOpened) {
            System.out.println(help + bibliotheques.keySet() + "\n\n");
            entree = getEntreesUtilisateur();
            try {
                switch (entree.t1) {
                    case 1:
                        ajouterBibliotheque(entree.t2.get(0));
                        break;
                    case 2:
                        ajouterNouveauDocument(entree.t2.get(0));
                        break;
                    case 3:
                        ajouterUtilisateur(entree.t2);
                        break;
                    case 4:
                        consulterEnsembleDocument(entree.t2.get(0));
                        break;
                    case 5:
                        consulterDocumentsMemeSerie(entree.t2);
                        break;
                    case 6:
                        consulterDocumentsMemeAuteur(entree.t2);
                        break;
                    case 7:
                        rechercheParIsbn(entree.t2);
                        break;
                    case 8:
                        rechercheParEan(entree.t2);
                        break;
                    case 9:
                        nbDeDocumentsParType(entree.t2);
                        break;
                    case 10:
                        utilisateurEmprunteDocument(entree.t2);
                        break;
                    case 11:
                        utilisateurRendDocument(entree.t2);
                        break;
                    case 12:
                        biblioEmprunteDocument(entree.t2);
                        break;
                    case -1:
                        isOpened = false;
                        break;
                }
            } catch (Exception e) {
                System.out.println(e.getClass().getSimpleName() + " : " + e.getMessage() + "\n");
            }
            if (isOpened) attendreReactionUtilisateur();
            else System.out.println("À bientôt !");
        }
    }

    /**
     * Fonctionnalité 1.
     * Ajouter une nouvelle bibliotheque dans le réseau à partir de son nom.
     * @param nomBibliotheque Le nom de la nouvelle bibliothèque.
     * @throws InvalidParameterException S'il n'y a pas de nom de bibliothèque passée en paramètre.
     * @throws ExceptionBibliotheque Si la bibliothèque existe déjà.
     */
    private void ajouterBibliotheque(String nomBibliotheque) throws ExceptionBibliotheque, InvalidParameterException {
        if (nomBibliotheque.equals(""))
            throw new InvalidParameterException("Veuillez fournir un nom de bibliothèque.");
        else if (bibliotheques.containsKey(nomBibliotheque))
            throw new ExceptionBibliotheque("Création impossible : Le nom de bibliothèque fourni existe déjà.");
        else
            bibliotheques.put(nomBibliotheque, new Bibliotheque(nomBibliotheque));
    }

    /**
     * Fonctionnalité 2.
     * Ajoute un document au réseau identifié par le paramètre donné. Si celui-ci est valide, l'utilisateur entre dans une boucle et doit
     * entrer les différentes valeurs qu'il souhaite donner au nouveau documement.
     * @param idDocument Identificateur du document que l'utilisateur veut ajouter.
     */
    private void ajouterNouveauDocument(String idDocument) {
        if (idDocument.equals(""))
            throw new InvalidParameterException("Veuillez spécifier un isbn ou un ean.");
        if ((FileReader.estISBN(idDocument) && livres.containsKey(idDocument)) || (documents.containsKey(idDocument)))
            throw new InvalidParameterException("Veuillez entrer un autre identifiant, celui que vous avez donné est déjà présent dans le réseau.");

        // Contient les attributs à remplir pour créer un nouveau document et l'ajouter au réseau
        HashMap<String, String> attributsEtValeurs = new HashMap<>();
        // Crée les clés avec des attributs vides
        for (String key : new String[]{"title", "publisher", "date", "authorName", "authorSurname", "totalCopies", "type"}) attributsEtValeurs.put(key,"");

        Scanner entrees = new Scanner(System.in);
        // On n'utilise pas le keySet car on veut garder l'ordre title, publisher etc
        for (String key : new String[]{"title", "publisher", "date", "authorName", "authorSurname", "totalCopies", "type"}) {
            System.out.println("Entrez la valeur que vous souhaitez pour l'attribut " + key + " :");
            if (key.equals("type")) System.out.println("Pour vous aider, les types possibles sont : livre, bande dessinee, carte, partition, vinyle, revue, jeux videos, jeux de societe, dvd.\nSi vous mettez autre chose, le type sera automatiquement mis à autres.");
            attributsEtValeurs.replace(key, entrees.nextLine());
        }

        try { // Vérification de la date
            int dateInt = Integer.parseInt(attributsEtValeurs.get("date"));
            if (dateInt%10000 >= 2021 || dateInt%10000 < 0) throw new DateTimeException("La date n'est pas valide.");
            else if (dateInt/10000 == 0) attributsEtValeurs.replace("date", Integer.toString(dateInt%10000));
            else attributsEtValeurs.replace("date", dateInt%10000 + "-" + dateInt/10000);
        } catch (Exception e) {
            throw new InvalidParameterException("Veuillez essayer à nouveau, la date " + attributsEtValeurs.get("date") + " est erronée car : " + e);
        }

        try { // Vérification du nombre de copies
            Integer.parseInt(attributsEtValeurs.get("totalCopies"));
        } catch (Exception e) {
            throw new InvalidParameterException("Veuillez essayer à nouveau, le nombre de copies " + attributsEtValeurs.get("totalCopies") + " est erronée car : " + e);
        }

        // Nous créons le nouveau document et l'ajoutons au réseau
        String isbn = "", ean = ""; // Il n'y aura qu'un seul identifiant possible pour ce document (cf sujet)
        if (FileReader.estISBN(idDocument)) isbn = idDocument;
        else ean = idDocument;
        Document doc = FileReader.genererDocumentSelonType(isbn, ean, attributsEtValeurs.get("title"), attributsEtValeurs.get("publisher"), attributsEtValeurs.get("date"), attributsEtValeurs.get("authorName"), attributsEtValeurs.get("authorSurname"), attributsEtValeurs.get("type"), Integer.parseInt(attributsEtValeurs.get("totalCopies")));

        if (!isbn.equals("")) livres.put(isbn, (Livre) doc);
        if (!ean.equals("")) documents.put(ean, doc);

        System.out.println("Voici un résumé du document ajouté :\n" + doc);
    }

    /**
     * Fonctionnalité 3.
     * Ajoute un utilisateur à une bibliothèque.
     * @param parametres Le nom de la bibliothèque associée à cet utilisateur,
     *                    et le nombre maximal de documents qu'il peut emprunter en même temps.
     * @throws ExceptionBibliotheque Si le nom de bibliothèque est mal renseigné.
     */
    private void ajouterUtilisateur(ArrayList<String> parametres) throws ExceptionBibliotheque {
        if (parametres.size() != 2)
            throw new InvalidParameterException("Veuillez fournir un nom de bibliothèque et un quota.");

        String nomBibliotheque = parametres.get(0);
        if (nomBibliotheque.equals("") || !bibliotheques.containsKey(nomBibliotheque))
            throw new ExceptionBibliotheque("Ajout impossible : La bibliothèque n'existe pas.");

        int quotaMaxDocuments = Integer.parseInt(parametres.get(1));
        Utilisateur utilisateur = new Utilisateur(bibliotheques.get(nomBibliotheque), quotaMaxDocuments);
        bibliotheques.get(nomBibliotheque).ajouterUtilisateur(utilisateur);
        this.utilisateurs.put(utilisateur.getId(), utilisateur);
        System.out.println("L'utilisateur n°" + utilisateur.getId() + " a été ajouté à la bibliothèque " + nomBibliotheque + " avec succès.");
    }

    /**
     * Fonctionnalité 4.
     * Affiche la totalité des documents et livres enregistrés dans l'ensemble du réseau si aucun nom de bibliothèque n'est renseigné (ou qu'il est mal entré par l'utilisateur).
     * Sinon, affiche la totalité des documents et livres enregistrés connus par une bibliothèque.
     * @param nomBibliotheque Nom de la bibliothèque dont l'utilisateur veut consulter l'ensemble des oeuvres.
     */
    private void consulterEnsembleDocument(String nomBibliotheque) {
        Bibliotheque biblio = bibliotheques.get(nomBibliotheque);

        List<String> allIds = new ArrayList<>(documents.keySet()); // Contient tous les identifiants connus du réseau
        allIds.addAll(livres.keySet());

        if (nomBibliotheque.equals(""))
            for (String key : allIds)
                System.out.println(getDocumentFromId(key));

        else if (biblio == null) { // Si le nom de bibliothèque est mal renseigné
            System.out.println("Le nom de bibliothèque renseigné n'est pas dans notre base, nous vous affichons l'ensemble des documents à la place.\n");
            attendreReactionUtilisateur();
            for (String key : allIds)
                System.out.println(getDocumentFromId(key));
        }

        else
            for (String id : bibliotheques.get(nomBibliotheque).recupererTousLesIdentifiants())
                System.out.println(getDocumentFromId(id));
    }

    /**
     * Fonctionnalité 5.
     * Permet de consulter les documents d'une série par ordre de publication.
     * Il est également possible de faire la recherche dans la totalité du réseau ou dans une bibliothèque en particulier.
     * @param parametres Le nom de la série, et en dernière position, si voulu, la bibliothèque dans laquelle on veut faire la recherche.
     * @throws ExceptionSerie Si le nom de série fourni n'existe pas.
     */
    private void consulterDocumentsMemeSerie(ArrayList<String> parametres) throws ExceptionSerie {
        if (parametres.size() > 2)    // Vérification nombre de paramètres fournis.
            throw new InvalidParameterException("Veuillez ne fournir qu'un nom de série et éventuellement une bibliothèque.");
        // Si l'utilisateur ne donne aucun paramètre, ou alors juste le nom de la bibliothèque
        if (parametres.size() == 1 && (parametres.get(0).equals("") || bibliotheques.containsKey(parametres.get(parametres.size() - 1))))
            throw new InvalidParameterException("Veuillez fournir un titre de série.");

        Bibliotheque biblio = null;
        // Si l'utilisateur veut les résultats pour une bibliothèque en particulier
        if (bibliotheques.containsKey(parametres.get(parametres.size() - 1))) {
            biblio = bibliotheques.get(parametres.get(parametres.size() - 1));
            parametres.remove(parametres.size() - 1);
        }
        // On récupère le nom de la série
        StringBuilder nomSerie = new StringBuilder();
        for (String mot : parametres) {
            nomSerie.append(mot).append(" ");
        }
        nomSerie.deleteCharAt(nomSerie.length() - 1);
        if (!series.containsKey(nomSerie.toString()))
            throw new ExceptionSerie("Série inexistante : " + nomSerie.toString());

        Serie serie = series.get(nomSerie.toString());

        if (biblio != null) { // recherche pour une bibliotheque en particulier
            List<String> allIdsBiblio = biblio.recupererTousLesIdentifiants();
            for (Integer numEpisode : serie.getAllNumberEpisodes()) {
                Document episode = serie.getSeriesEpisode(numEpisode);
                if ((episode instanceof Livre && allIdsBiblio.contains(((Livre) episode).getIsbn()))
                        || allIdsBiblio.contains(episode.getEan()))
                    System.out.println(serie.getSeriesEpisode(numEpisode));
            }
        } else { // recherche dans tout le réseau
            for (Integer numEpisode : serie.getAllNumberEpisodes()) {
                System.out.println(serie.getSeriesEpisode(numEpisode));
            }
        }
    }

    /**
     * Fonctionnalité 6.
     * Permet de consulter des documents ou livres en recherchant leur auteur par son nom, son prénom ou les deux en même temps.
     * Il est également possible de faire la recherche dans la totalité du réseau ou dans une bibliothèque en particulier.
     * @param parametres Contient d'abord le nom / prénom ou les deux de l'autheur, et en dernière position, si voulu, la bibliothèque dans laquelle on veut faire la recherche.
     */
    private void consulterDocumentsMemeAuteur(ArrayList<String> parametres) {
        if (parametres.size() > 3)     // Vérification nombre de paramètres fournis.
            throw new InvalidParameterException("Veuillez ne fournir au maximum un nom et prénom d'autheur et éventuellement une bibliothèque.");
        // Si l'utilisateur donne aucun paramètre, ou alors juste le nom de la bibliothèque
        if (parametres.size() == 1 && (parametres.get(0).equals("") || bibliotheques.containsKey(parametres.get(parametres.size() - 1))))
            throw new InvalidParameterException("Veuillez au moins fournir un nom ou prénom d'autheur.");

        List<String> allIds; // Liste des identifiants que l'on va afficher

        // Si l'utilisateur veut les résultats pour une bibliothèque en particulier
        if (bibliotheques.containsKey(parametres.get(parametres.size() - 1))) {
            Bibliotheque biblio = bibliotheques.get(parametres.get(parametres.size() - 1));
            parametres.remove(parametres.size() - 1);
            allIds = biblio.recupererTousLesIdentifiants();

        } else { // il veut pour le réseau entier
            allIds = new ArrayList<>(documents.keySet()); // Contient tous les identifiants connus du réseau
            allIds.addAll(livres.keySet());
        }

        ArrayList<Document> docsDejaAffiches = new ArrayList<>();

        switch (parametres.size()) {
            case 1: // Only name or surname
                for (String key : allIds) {
                    Document doc = getDocumentFromId(key);
                    if ((doc.getAuthorName().contains(parametres.get(0)) || doc.getAuthorSurname().contains(parametres.get(0))) && !docsDejaAffiches.contains(doc)) {
                        System.out.println(doc);
                        docsDejaAffiches.add(doc);
                    }
                }
                break;

            case 2: // Both surname and name are given
                for (String key : allIds) { // Puis affiche la totalité des livres
                    Document doc = getDocumentFromId(key);
                    // On vérifie en prenant en compte que l'ordre soit nom puis prénom ou prénom puis nom
                    if (((doc.getAuthorName().contains(parametres.get(0)) && doc.getAuthorSurname().contains(parametres.get(1))) || (doc.getAuthorName().contains(parametres.get(1)) && doc.getAuthorSurname().contains(parametres.get(0)))) && !docsDejaAffiches.contains(doc)) {
                        System.out.println(doc);
                        docsDejaAffiches.add(doc);
                    }
                }
                break;

            default:
                throw new InvalidParameterException("Veuillez n'entrer qu'un nom, un prénom ou les deux, pas plus.");
        }
    }

    /**
     * Permet de factoriser le code pour la recherche par isbn et la recherche par ean.
     * @param parametres L'ISBN du livre ou l'EAN du document, suivi éventuellement d'un nom de bibliothèque.
     * @param isbn Pour savoir si l'appel de cette méthode vient de rechercheParIsbn ou non.
     * @throws ExceptionDocument Si le livre / document n'est pas présent dans le réseau ou dans la bibliothèque demandée.
     * @throws ExceptionBibliotheque Si la bibliothèque demandée n'existe pas.
     */
    private void checkIsbnEan(ArrayList<String> parametres, boolean isbn) throws ExceptionDocument, ExceptionBibliotheque {
        if (parametres.size() > 2)    // Vérification nombre de paramètres fournis.
            throw new InvalidParameterException("Veuillez fournir uniquement un ISBN et éventuellement une bibliothèque.");
        if (parametres.size() == 1 && (parametres.get(0).equals("") || bibliotheques.containsKey(parametres.get(0))))
            throw new InvalidParameterException("Veuillez fournir un ISBN.");

        String id = parametres.get(0);
        if (isbn && !livres.containsKey(id))    // Si le livre n'existe pas
            throw new ExceptionDocument(id + " : Identifiant inexistant dans le réseau.");
        else if (!isbn && !documents.containsKey(id))
            throw new ExceptionDocument(id + " : Identifiant inexistant dans le réseau.");

        if (parametres.size() == 2) {   // Si recherche pour une bibliothèque en particulier
            if (!bibliotheques.containsKey(parametres.get(1)))
                throw new ExceptionBibliotheque(parametres.get(1) + " : La bibliothèque spécifiée n'existe pas.");

            Bibliotheque biblio = bibliotheques.get(parametres.get(1));
            if (biblio.getQuantite(id) == -1)   // Si le document existe mais pas dans la biblio fournie
                throw new ExceptionDocument("Le document n° " + id + " existe, mais n'est pas présent dans la bibliothèque " + biblio.getName());
        }
    }

    /**
     * Fonctionnalité 7.
     * Recherche d'un livre par son ISBN.
     * @param parametres L'ISBN du livre, suivi éventuellement d'un nom de bibliothèque.
     * @throws ExceptionDocument Si le livre n'est pas présent dans le réseau ou dans la bibliothèque demandée.
     * @throws ExceptionBibliotheque Si la bibliothèque demandée n'existe pas.
     */
    private void rechercheParIsbn(ArrayList<String> parametres) throws ExceptionDocument, ExceptionBibliotheque {
        checkIsbnEan(parametres, true);

        System.out.println(livres.get(parametres.get(0)));   // Affichage du livre
    }

    /**
     * Fonctionnalité 8.
     * Recherche d'un document par son EAN.
     * @param parametres L'EAN du document, suivi éventuellement d'un nom de bibliothèque.
     * @throws ExceptionDocument Si le livre n'est pas présent dans le réseau ou dans la bibliothèque demandée.
     * @throws ExceptionBibliotheque Si la bibliothèque demandée n'existe pas.
     */
    private void rechercheParEan(ArrayList<String> parametres) throws ExceptionDocument, ExceptionBibliotheque {
        checkIsbnEan(parametres, false);

        System.out.println(documents.get(parametres.get(0)));   // Affichage du document
    }

    /**
     * Fonctionnalité 9.
     * Affiche le nombre de documents de chaque type publiés dans un intervalle de temps.
     * Si aucun intervalle n'est donné, l'affichage est fait pour l'ensemble des documents.
     * La recherche peut être restreinte à une bibliothèque.
     * @param parametres Peut contenir une année de début ; une année de début et une bibliothèque ;
     *                  une année de début et une année de fin ; les 3 ; ou rien du tout.
     * @throws ExceptionBibliotheque Si la bibliothèque spécifiée n'existe pas.
     */
    private void nbDeDocumentsParType(ArrayList<String> parametres) throws ExceptionBibliotheque {
        // Par défaut, recherche sur tout le réseau
        Integer anneeDebut = null,
                anneeFin   = null;
        Bibliotheque biblio = null;

        // Lecture des différents paramètres
        for (String mot : parametres) {
            // Si le paramètre est une année
            if (mot.matches("\\d+")) {
                // On vérifie si pas encore renseignée : si oui, on l'ajoute, sinon exception
                if    (anneeDebut == null) anneeDebut = Integer.parseInt(mot);
                else if (anneeFin == null) anneeFin = Integer.parseInt(mot);
                else throw new InvalidParameterException("Veuillez fournir au plus une année de début et une année de fin.");
            }
            // Si le paramètre est une biblio
            else if (!mot.equals("")) {
                // On vérifie si pas encore renseignée : si oui, on l'ajoute, sinon exception
                if (biblio == null) {
                    if (bibliotheques.containsKey(mot))
                        biblio = bibliotheques.get(mot);
                    else throw new ExceptionBibliotheque(mot + " : La bibliothèque spécifiée n'existe pas.");
                }
                else throw new InvalidParameterException("Veuiller fournir au plus une bibliothèque.");
            }
        }

        // Ajout de bornes si non renseignées et inversion si nécessaire
        if (anneeDebut == null) anneeDebut = 0;
        if (anneeFin == null)   anneeFin = Calendar.getInstance().get(Calendar.YEAR);
        if (anneeDebut > anneeFin) {
            int temp = anneeDebut;
            anneeDebut = anneeFin;
            anneeFin = temp;
        }

        // Comptage des documents
        int nbDocument = 0, nbLivre = 0, nbAutre = 0, nbBD = 0, nbCarte = 0, nbPartition = 0, nbCD = 0, nbJeuDeSociete = 0, nbJeuVideo =0, nbRevue = 0, nbVinyle = 0;
        for (String idDoc : documents.keySet()) {
            Document doc = documents.get(idDoc);
            int anneeDoc;
            try {
                anneeDoc = Integer.parseInt(doc.getDate());
            } catch (Exception e) {
                anneeDoc = -1;
            }
            if ((biblio == null || biblio.getQuantite(idDoc) != -1) && anneeDoc >= anneeDebut && anneeDoc <= anneeFin) {
                nbDocument++;
                if (doc instanceof Livre) nbLivre++;
                if (doc instanceof Autre) nbAutre++;
                if (doc instanceof BandeDessinee) nbBD++;
                if (doc instanceof Carte) nbCarte++;
                if (doc instanceof Partition) nbPartition++;
                if (doc instanceof CD) nbCD++;
                if (doc instanceof JeuDeSociete) nbJeuDeSociete++;
                if (doc instanceof JeuVideo) nbJeuVideo++;
                if (doc instanceof Revue) nbRevue++;
                if (doc instanceof Vinyle) nbVinyle++;
            }
        }

        // Affichage des résultats
        String out = "Nombre de documents publiés de " + anneeDebut + " à " + anneeFin;
        if (biblio != null)
            out += " dans la bibliothèque " + biblio.getName();
        out += " : " + nbDocument
                + "\nDont : \n"
                + nbLivre + " Livres\n"
                + nbBD + " Bandes Dessinées\n"
                + nbCarte + " Cartes\n"
                + nbPartition + " Partitions\n"
                + nbCD + " CDs\n"
                + nbJeuDeSociete + " Jeux de Société\n"
                + nbJeuVideo + " Jeux vidéo\n"
                + nbRevue + " Revues\n"
                + nbVinyle + " Vinyles\n"
                + nbAutre + " Autres\n";

        System.out.println(out);
    }

    /**
     * Permet de factoriser le code pour l'emprunt et la remise de documents par un utilisateur.
     * @param parametres Doit contenir : un identifiant de document, d'utilisateur et le nom d'une bibliothèque.
     * @return Une paire contenant d'abord l'identifiant du document, puis l'identifiant de l'utilisateur.
     * @throws ExceptionEmpruntRemise Si le document n'existe pas, si l'identifiant de l'utilisateur est incorrect.
     */
    private Pair<String, Integer> checkEmpruntRemise(ArrayList<String> parametres) throws ExceptionEmpruntRemise {
        if (parametres.size() != 2)
            throw new InvalidParameterException("Veuillez donner exactement 2 paramètres, identifiantDocument identifiantUtilisateur");

        Integer id = Integer.parseInt(parametres.get(1));

        if (!utilisateurs.containsKey(id))
            throw new ExceptionEmpruntRemise(parametres.get(1) + " : L'utilisateur spécifié n'existe pas.");

        ArrayList<String> allIds;
        allIds = new ArrayList<>(documents.keySet()); // Contient tous les identifiants connus du réseau
        allIds.addAll(livres.keySet());

        if (!allIds.contains(parametres.get(0)))
            throw new InvalidParameterException(parametres.get(0) + " : Le document spécifié n'existe pas.");

        return new Pair<>(parametres.get(0), id);
    }

    /**
     * Fonctionnalité 10.
     * Permet à un utilisateur d'emprunter un document à une bibliothèque.
     * @param parametres Doit contenir : un identifiant de document, d'utilisateur et le nom d'une bibliothèque.
     * @throws InvalidParameterException S'il n'y a pas 2 paramètres.
     * @throws ExceptionEmpruntRemise Si le document n'existe pas, si l'identifiant de l'utilisateur est incorrect, s'il n'y a plus d'exemplaires dispos, si l'utilisateur a dépassé son quota d'emprunts.
     */
    private void utilisateurEmprunteDocument(ArrayList<String> parametres) throws InvalidParameterException, ExceptionEmpruntRemise {
        Pair<String, Integer> u = checkEmpruntRemise(parametres);

        utilisateurs.get(u.t2).getBibliotheque().preterDocumentAUtili(getDocumentFromId(u.t1), u.t2);
    }

    /**
     * Fonctionnalité 11.
     * Permet à un utilisateur de rendre un document à une bibliothèque.
     * @param parametres Doit contenir : un identifiant de document, d'utilisateur et le nom d'une bibliothèque.
     * @throws InvalidParameterException S'il n'y a pas 2 paramètres.
     * @throws ExceptionEmpruntRemise Si l'identifiant d'utilisateur est incorrect, si le document n'est pas dans la liste des documents empruntés par l'utilisateur.
     */
    private void utilisateurRendDocument(ArrayList<String> parametres) throws InvalidParameterException, ExceptionEmpruntRemise {
        Pair<String, Integer> u = checkEmpruntRemise(parametres);

        utilisateurs.get(u.t2).getBibliotheque().rendreDocument(getDocumentFromId(u.t1), u.t2);
    }

    /**
     * Fonctionnalité 12.
     * Permet à une bibliothèque d'emprunter un document à une autre bibliothèque.
     * @param parametres Doit contenir :  l'identifiant du document,
     *                   le nom de la bibliothèque qui emprunte,
     *                   le nom de la bibliothèque qui prête.
     * @throws InvalidParameterException S'il n'y a pas 3 paramètres.
     * @throws ExceptionBibliotheque Si au moins l'une des bibliothèques renseignées n'existe pas.
     * @throws ExceptionEmpruntRemise Si le document n'existe pas ou s'il n'y a plus d'exemplaires dispos.
     */
    private void biblioEmprunteDocument(ArrayList<String> parametres) throws InvalidParameterException, ExceptionBibliotheque, ExceptionEmpruntRemise {
        if (parametres.size() != 3)
            throw new InvalidParameterException("Veuillez donner exactement 3 paramètres, identifiantDocument nomBibliothèqueReceveuse nomBibliothèqueDonneuse");
        if (!bibliotheques.containsKey(parametres.get(1)))
            throw new ExceptionBibliotheque(parametres.get(1) + " : La bibliothèque spécifiée n'existe pas.");
        if (!bibliotheques.containsKey(parametres.get(2)))
            throw new ExceptionBibliotheque(parametres.get(2) + " : La bibliothèque spécifiée n'existe pas.");

        ArrayList<String> allIds;
        allIds = new ArrayList<>(documents.keySet()); // Contient tous les identifiants connus du réseau
        allIds.addAll(livres.keySet());

        if (!allIds.contains(parametres.get(0)))
            throw new InvalidParameterException(parametres.get(0) + " : Le document spécifié n'existe pas.");

        bibliotheques.get(parametres.get(2)).envoyerDocumentABiblio(getDocumentFromId(parametres.get(0)), bibliotheques.get(parametres.get(1)));
    }

    /**
     * Récupère le numéro de la commande demandée par l'utilisateur et le paramètre optionnel s'il y en a un.
     * @return Une instance de Pair qui comporte deux champs, le numéro de commande (int) et le paramètre optionnel (String).
     */
    private Pair<Integer, ArrayList<String>> getEntreesUtilisateur() {
        Scanner in = new Scanner(System.in);
        int commande;
        ArrayList<String> paramsOptionnels;
        try {
            commande = in.nextInt();
            paramsOptionnels = new ArrayList<>(Arrays.asList(in.nextLine().split("\\s+")));
            if (paramsOptionnels.size() > 1 && paramsOptionnels.get(0).equals("")) paramsOptionnels.remove(0);
            if (paramsOptionnels.isEmpty()) paramsOptionnels.add("");
        } catch (java.util.InputMismatchException e) {
            System.out.println("Vous avez entré autre chose qu'un entier entre 1 et 9. Veuillez recommencer.");
            return getEntreesUtilisateur();
        }
        return new Pair<>(commande, paramsOptionnels);
    }

    /**
     * Permet juste d'attendre que l'utilisateur lise le message et presse Entrée pour continuer.
     */
    private void attendreReactionUtilisateur() {
        System.out.println("Appuyez sur \"Entrée\" pour continuer...\n");
        Scanner wait = new Scanner(System.in);
        wait.nextLine(); // Juste pour attendre une réaction de l'utilisateur
    }

    /**
     * Rend un document après l'avoir cherché dans la bonne map en fonction de si l'identifiant donné était un isbn ou ean.
     * @param idDocument Id du document voulu.
     * @return Le document.
     */
    private Document getDocumentFromId(String idDocument) {
        if (FileReader.estISBN(idDocument)) return livres.get(idDocument);
        return documents.get(idDocument);
    }
}