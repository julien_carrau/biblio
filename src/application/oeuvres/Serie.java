package application.oeuvres;

import application.oeuvres.livres.Livre;

import java.util.HashMap;
import java.util.Set;

/**
 * Classe représentant une série de documents.
 * Elle référence tous ces documents.
 * L'ensemble des séries sera enregistré dans une HashMap.
 */
public class Serie {
    private String seriesTitle;
    private HashMap<Integer, Document> episodes;

    /**
     * Constructeur de la classe.
     * @param seriesTitle Titre de la série.
     */
    public Serie(String seriesTitle) {
        this.seriesTitle = seriesTitle;
        this.episodes = new HashMap<>();
    }

    /**
     * Méthode d'accès au titre de la série.
     * @return Le titre de la série.
     */
    public String getSeriesTitle() { return seriesTitle; }

    /**
     * Méthode d'accès à un épisode de la série par son numéro.
     * @param numeroEpisode Le numéro de l'épisode.
     * @return Cet épisode.
     */
    public Document getSeriesEpisode(int numeroEpisode) { return episodes.get(numeroEpisode); }

    /**
     * Méthode d'accès à tous les numéros de cette série.
     * @return Un Set contenant tous ces numéros.
     */
    public Set<Integer> getAllNumberEpisodes() { return episodes.keySet(); }

    /**
     * Méthode permettant d'ajouter un épisode à la série.
     * @param numeroEpisode Le numéro de l'épisode. S'il est déjà présent dans la série, il sera incrémenté d'autant qu'il faut.
     * @param episode L'épisode à ajouter.
     */
    public void ajouterEpisode(int numeroEpisode, Document episode) {
        if (!episodes.containsKey(numeroEpisode)) {
            episodes.put(numeroEpisode, episode);
        }
        while (episodes.containsKey(numeroEpisode)) numeroEpisode++;
        episodes.put(numeroEpisode, episode);
    }

    /**
     * Renvoie une représentation textuelle de la série,
     * contenant son titre et son nombre d'épisodes.
     * @return Cette représentation textuelle.
     */
    @Override
    public String toString() {
        return "Series title : " + getSeriesTitle() + "\n" +
                "Nombre d'épisodes : " + episodes.size() + "\n";
    }
}
