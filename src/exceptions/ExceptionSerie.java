package exceptions;

/**
 * Exception concernant la gestion de séries.
 */
public class ExceptionSerie extends Exception {
    public ExceptionSerie(String message) {
        super(message);
    }
}
