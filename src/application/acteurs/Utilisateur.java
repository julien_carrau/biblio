package application.acteurs;

import application.oeuvres.Document;

import java.util.ArrayList;

/**
 * Classe modélisant un utilisateur inscrit dans une bibliothèque.
 * Il dispose d'un identifiant unique dans tout le réseau.
 */
public class Utilisateur {
    private static int idActuel = 0;
    private final int id;
    private final Bibliotheque bibliotheque;
    private final int quotaMaxDocuments;
    private ArrayList<Document> documentsEmpruntes;

    /**
     * Constructeur de la classe.
     * @param bibliotheque la bibliothèque associée à l'utilisateur.
     * @param quotaMaxDocuments le nombre maximal de documents que peut emprunter l'utilisateur en même temps.
     */
    public Utilisateur(Bibliotheque bibliotheque, int quotaMaxDocuments) {
        this.id = idActuel;
        idActuel++;
        this.bibliotheque = bibliotheque;
        this.quotaMaxDocuments = quotaMaxDocuments;
        this.documentsEmpruntes = new ArrayList<>();
    }

    /**
     * Méthode d'accès à l'identifiant (unique) de l'utilisateur.
     * @return l'identifiant de l'utilisateur.
     */
    public int getId() {
        return id;
    }

    /**
     * Méthode d'accès à la bibliothèque associée à l'utilisateur.
     * @return la bibliothèque associée à l'utilisateur.
     */
    public Bibliotheque getBibliotheque() {
        return bibliotheque;
    }

    /**
     * Méthode d'accès au quota maximum de documents que peut emprunter l'utilisateur en même temps.
     * @return le quota maximum de documents que peut emprunter l'utilisateur en même temps.
     */
    public int getQuotaMaxDocuments() {
        return quotaMaxDocuments;
    }

    /**
     * Donne le nombre de documents actuellement empruntés par l'utilisateur.
     * @return Le nombre de documents actuellement empruntés.
     */
    public int getNombreDocumentsEmpruntes() { return documentsEmpruntes.size(); }

    /**
     * Retourne la liste des documents empruntés par l'utilisateur.
     * @return La liste des documents empruntés par l'utilisateur.
     */
    protected ArrayList<Document> getDocumentsEmpruntes() { return documentsEmpruntes; }

    /**
     * Ajoute l'identifiant dans la liste des documents empruntés de l'utilisateur.
     * @param document Document emprunté.
     */
    protected void emprunter(Document document) {
        documentsEmpruntes.add(document);
        System.out.println("Document emprunté avec succès !\n");
    }

    /**
     * Rend le document.
     * @param document Document à rendre.
     */
    protected void rendre(Document document) {
        documentsEmpruntes.remove(document);
        System.out.println("Document rendu avec succès !\n");
    }

    /**
     * Renvoie une représentation textuelle de l'utilisateur, contenant
     * son identifiant, sa bibliothèque associée, son quota de documents empruntables,
     * et la liste de ses documents empruntés actuellement.
     * @return Cette représentation textuelle.
     */
    @Override
    public String toString() {
        StringBuilder docs = new StringBuilder();
        for (Document doc : documentsEmpruntes) docs.append(doc.toString()).append("\n");
        return "Utilisateur n° " + id +
                "\n\tBibliothèque : " + bibliotheque.getName() +
                "\n\tQuota maximal de documents : " + quotaMaxDocuments +
                "\n\tDocuments empruntés : \n" + docs;
    }
}
