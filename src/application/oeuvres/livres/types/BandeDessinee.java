package application.oeuvres.livres.types;

import application.oeuvres.livres.Livre;

/**
 * Classe héritant de Livre et représentant une bande dessinée.
 */
public class BandeDessinee extends Livre {
    /**
     * @param ean Numéro d'EAN unique identifiant le document. @see <a href="https://fr.wikipedia.org/wiki/EAN_13">Page Wikipedia sur l'ean.</a>
     * @param isbn Numéro d'ISBN unique du livre. @see <a href="https://fr.wikipedia.org/wiki/International_Standard_Book_Number">Page Wikipedia sur l'isbn.</a>
     * @param title Titre du document.
     * @param publisher Publicateur du document.
     * @param date Date de publication.
     * @param authorName Nom de l'autheur.
     * @param authorSurname Prénom de l'autheur.
     * @param totalCopies Nombre total de copies disponibles réparties entre les différentes bibliothèques de paris.
     * @see application.oeuvres.livres.Livre#Livre(String, String, String, String, String, String, String, int)
     */
    public BandeDessinee(String ean, String isbn, String title, String publisher, String date, String authorName, String authorSurname, int totalCopies) {
        super(ean, isbn, title, publisher, date, authorName, authorSurname, totalCopies);
    }
}
