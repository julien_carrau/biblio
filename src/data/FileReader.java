package data;

import application.acteurs.Bibliotheque;
import application.oeuvres.Document;
import application.oeuvres.Serie;
import application.oeuvres.types.Revue;
import application.oeuvres.livres.Livre;
import application.oeuvres.livres.types.*;
import application.oeuvres.types.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Classe utilisée pour la création d'un réseau de bibliothèques
 * et l'analyse des données brutes d'un fichier CSV.
 */
public class FileReader {
	private static class Pair {
		public ArrayList<String> isbnList, eanList;
		public Pair(ArrayList<String> isbnList, ArrayList<String> eanList) {
			this.isbnList = isbnList;
			this.eanList = eanList;
		}
	}

	/**
	 * Classe contenant des HashMaps des documents, des livres, des séries et des bibliothèques d'un réseau.
	 */
	public static class ResultatGetData {
		public HashMap<String, Document> documents;
		public HashMap<String, Livre> livres;
		public HashMap<String, Serie> series;
		public HashMap<String, Bibliotheque> bibliotheques;

		public ResultatGetData(HashMap<String, Document> documents, HashMap<String, Livre> livres, HashMap<String, Serie> series, HashMap<String, Bibliotheque> bibliotheques) {
			this.documents = documents;
			this.livres = livres;
			this.series = series;
			this.bibliotheques = bibliotheques;
		}
	}

	/**
	 * Méthode permettant de récupérer les données d'un fichier CSV et de créer le réseau correspondant.
	 * @param csvFilePath Le chemin d'accès du fichier CSV.
	 * @return Un objet contenant les HashMap des documents, des livres, des séries et des bibliothèques du réseau.
	 */
	public static ResultatGetData getDataFromCSVFile(String csvFilePath) { // List<?> pour éviter un warning -> Raw use of ...
		HashMap<String, Document> documents = new HashMap<>();
		HashMap<String, Livre> livres = new HashMap<>();
		HashMap<String, Serie> series = new HashMap<>();
		HashMap<String, Bibliotheque> bibliotheques = new HashMap<>();

		// Initialisation des bibliothèques
		for (String biblio : new String[]{"AimeCesaire", "EdmondRostand", "JeanPierreMelville", "OscarWilde", "SaintSimon"})
			bibliotheques.put(biblio, new Bibliotheque(biblio));

        String line;
        String[] data;
        String separator = ";(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)";
        
        //Document data
        String isbn;
        String ean;
        String title;
        String publisher;
        String date;
        String seriesTitle;
        String authorName;
        String authorSurname;
        String type;
        int seriesNumber;
        int totalCopies;
        int numberCopyAimeCesaire;
        int numberCopyEdmondRostand;
        int numberCopyJeanPierreMelville;
        int numberCopyOscarWilde;
        int numberCopySaintSimon;
        
        try (BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(csvFilePath), StandardCharsets.ISO_8859_1)) {
        	//Read the first line
        	line = bufferedReader.readLine();
        	
        	//Get data from the line
        	data = line.split(separator, -1);
        	
        	if (data.length != 16) {
        		System.out.println("[FileReader] The file at " + csvFilePath + " does not contain the right number of columns.");
        		throw new IOException("[FileReader] The file at " + csvFilePath + " does not contain the right number of columns.");
        	}
        	
        	//Read the file line by line
            while ((line = bufferedReader.readLine()) != null) {
            	//Get data from the line
            	data = line.split(separator, -1);
            	
            	//Sort data
            		
            		//Get the ISBN number
            		isbn = data[0];
            		
            		//Get the EAN number
            		ean = data[1];
            		
            		//Get the title of the document
            		title = data[2];

            		//Get the name of the publisher
            		publisher = data[3];
            		
            		//Get the publication date
            		try {
            			int dateInt = Integer.parseInt(data[4].replaceAll("[^0-9]", ""));
            			
            			if(dateInt%10000 >= 2021 || dateInt%10000 < 0) date = "?";
            			else if(dateInt/10000 == 0) date = Integer.toString(dateInt%10000);
            			else date = dateInt%10000 + "-" + dateInt/10000;
            		} catch (Exception exception) {
            			date = "?";
					}

            		
            		//Get the title of the series
            		seriesTitle = data[5];
            		
            		//Get the number of this document in the series
            		try {
            			seriesNumber = Integer.parseInt(data[6]);
            		}
            		catch (Exception exception) {
            			seriesNumber = 1;
            		}
            		
            		//Get the name of the author
            		authorSurname = data[7];
            		
            		//Get the surname of the author
            		authorName = data[8];
            		
            		//Get the type of the document
            		type = data[9];
            		
            		//Get the total number of copy in Paris for this document 
            		try {
            			totalCopies = Integer.parseInt(data[10]);
            		} catch (Exception exception) {
            			totalCopies = 0;
            		}
            		
            		//Get the number of copy in the library "Aime Cesaire"
            		try {
            			numberCopyAimeCesaire = Integer.parseInt(data[11]);
            		} catch (Exception exception) {
            			numberCopyAimeCesaire = 0;
            		}
            		
            		//Get the number of copy in the library "Edmond Rostand"
            		try {
            			numberCopyEdmondRostand = Integer.parseInt(data[12]);
            		} catch (Exception exception) {
            			numberCopyEdmondRostand = 0;
            		}
            		
            		//Get the number of copy in the library "Jean-Pierre Melville"
            		try {
            			numberCopyJeanPierreMelville = Integer.parseInt(data[13]);
            		} catch (Exception exception) {
            			numberCopyJeanPierreMelville = 0;
            		}
            		
            		//Get the number of copy in the library "Oscar Wilde"
            		try {
            			numberCopyOscarWilde = Integer.parseInt(data[14]);
            		} catch (Exception exception) {
            			numberCopyOscarWilde = 0;
            		}
            		
            		//Get the number of copy in the library "Saint-Simon"
            		try {
            			numberCopySaintSimon = Integer.parseInt(data[15]);
            		} catch (Exception exception) {
            			numberCopySaintSimon = 0;
            		}

            	// On corrige les erreurs d'ISBN et d'EAN
				Pair switched = corrigeIsbnEtEan(isbn, ean);

            	// On reforme l'isbn et l'ean une fois ceux-ci corrigés
				isbn = reformerIsbnOuEan(switched.isbnList);
				ean = reformerIsbnOuEan(switched.eanList);

				// On ne fait toute la suite que si le document a au moins un identifiant non vide
				String regex = "(0|1|2|3|4|5|6|7|8|9|-)+";
				if (isbn.matches(regex) || ean.matches(regex)) {
					// Récupérer le document en fonction de son type
					Document document = genererDocumentSelonType(isbn, ean, title, publisher, date, authorName, authorSurname, type, totalCopies);

					// On ajoute à livres si l'isbn est renseigné
					if (switched.isbnList.size() > 0 && !isbn.equals("")) { // Si le document a au moins un isbn
						for (String key : switched.isbnList) {
							try {
								livres.put(key, (Livre) document);
							} catch (ClassCastException c) {}
						}
					}

					// On ajoute à documents si l'ean est renseigné
					if (switched.eanList.size() > 0 && !ean.equals("")) { // Si le document a au moins un ean
						for (String key : switched.eanList) documents.put(key, document); // S'il y en a qu'un seul
					}

					// Si le livre fait partie d'une série
					if (!seriesTitle.equals("")) {
						if (!series.containsKey(seriesTitle)) // Si la série est déjà créée, on ajoute l'élément
							series.put(seriesTitle, new Serie(seriesTitle));
						series.get(seriesTitle).ajouterEpisode(seriesNumber, document);
					}

					// On regroupe toutes les clés, car la bibliothèque fera elle-même la différence entre les clés
					List<String> allId = new ArrayList<>();
					allId.addAll(switched.isbnList);
					allId.addAll(switched.eanList);
					// Ajout du nombre d'exemplaires dans les bibliothèques
					for (String id : allId) {
						if (numberCopyAimeCesaire != 0) bibliotheques.get("AimeCesaire").ajouterDocument(id);
						if (numberCopyEdmondRostand != 0) bibliotheques.get("EdmondRostand").ajouterDocument(id);
						if (numberCopyJeanPierreMelville != 0) bibliotheques.get("JeanPierreMelville").ajouterDocument(id);
						if (numberCopyOscarWilde != 0) bibliotheques.get("OscarWilde").ajouterDocument(id);
						if (numberCopySaintSimon != 0) bibliotheques.get("SaintSimon").ajouterDocument(id);
					}
				}
            }
        } catch (IOException exception) {
			System.err.println(exception);
		}

        return new ResultatGetData(documents, livres, series, bibliotheques);
	}

	/**
	 * Prend toutes les valeurs récupérées dans toutes les colonnes du csv et retourne une instance de la classe correspondant au type du document.
	 * @param isbn Numéro d'ISBN.
	 * @param ean Numéro d'EAN.
	 * @param title Titre.
	 * @param publisher Publicateur.
	 * @param date Date de parution.
	 * @param authorName Nom de l'autheur.
	 * @param authorSurname Prénom de l'autheur.
	 * @param type Type du document.
	 * @param totalCopies Nombre de copies disponibles dans Paris.
	 * @return Document, de la bonne classe.
	 * @see application.oeuvres.Document#Document(String, String, String, String, String, String, int)
	 */
	public static Document genererDocumentSelonType(String isbn, String ean, String title, String publisher, String date, String authorName, String authorSurname, String type, int totalCopies) {
		if (type.length() >= 5 && type.substring(0,5).toLowerCase().equals("livre"))
			return new Livre(ean, isbn, title, publisher, date, authorName, authorSurname, totalCopies);
		else if (type.length() >= 14 && type.substring(0,14).toLowerCase().equals("bande dessinee"))
			return new BandeDessinee(ean, isbn, title, publisher, date, authorName, authorSurname, totalCopies);
		else if (type.length() >= 5 && type.substring(0,5).toLowerCase().equals("carte"))
			return new Carte(ean, isbn, title, publisher, date, authorName, authorSurname, totalCopies);
		else if (type.length() >= 9 && type.substring(0,9).toLowerCase().equals("partition"))
			return new Partition(ean, isbn, title, publisher, date, authorName, authorSurname, totalCopies);
		else if (type.length() >= 6 && type.substring(0,6).toLowerCase().equals("vinyle"))
			return new Vinyle(ean, title, publisher, date, authorName, authorSurname, totalCopies);
		else if (type.length() >= 5 && type.substring(0,5).toLowerCase().equals("revue"))
			return new Revue(ean, title, publisher, date, authorName, authorSurname, totalCopies);
		else if (type.length() >= 11 && type.substring(0,11).toLowerCase().equals("jeux videos"))
			return new JeuVideo(ean, title, publisher, date, authorName, authorSurname, totalCopies);
		else if (type.length() >= 15 && type.substring(0,15).toLowerCase().equals("jeux de societe"))
			return new JeuDeSociete(ean, title, publisher, date, authorName, authorSurname, totalCopies);
		else if (type.length() >= 3 && type.substring(0,3).toLowerCase().equals("dvd"))
			return new CD(ean, title, publisher, date, authorName, authorSurname, totalCopies);
		else
			return new Autre(isbn, ean, title, publisher, date, authorName, authorSurname, totalCopies);
	}

	/**
	 * Corrige l'ISBN et l'EAN s'ils sont mal renseignés. On considère qu'un identifiant est un ISBN s'il contient au moins
	 * un "-". La méthode suivie est :
	 * 		1) On regroupe tous les identifiants confondus dans une seule liste toIterate (en splitant avec les ; s'il y a plusieurs identifiants renseignés)
	 * 		2) On parcout cette liste et en fonction de si l'identifiant est un isbn ou non, on l'ajoute dans isbnList ou eanList respectivement.
	 * @param isbn Le String isbn récupéré dans le csv.
	 * @param ean Le String ean récupéré dans le csv.
	 * @return Une instance de Pair ayant pour attributs la liste des isbn validés, et celle des ean validés.
	 */
	public static Pair corrigeIsbnEtEan(String isbn, String ean) {
		ArrayList<String> isbnList = new ArrayList<>();
		ArrayList<String> eanList = new ArrayList<>();
		ArrayList<String> toIterate = new ArrayList<>();

		// S'il y a plusieurs identifiants, on les sépare en supprimant les guillements de début et de fin du String (liés au csv)
		if (ean.contains(";")) toIterate.addAll(Arrays.asList(ean.substring(1, ean.length()-1).split(";")));
		else toIterate.add(ean);
		if (isbn.contains(";")) toIterate.addAll(Arrays.asList(isbn.substring(1, isbn.length()-1).split(";")));
		else toIterate.add(isbn);

		for (String item : toIterate) {
			if (!item.equals("") && estISBN(item)) {
				if (!isbnList.contains(item)) isbnList.add(item);
			}
			if (!item.equals("") && !estISBN(item)) {
				if (!eanList.contains(item)) eanList.add(item);
			}
		}
		return new Pair(isbnList, eanList);
	}

	/**
	 * Avec la liste d'identifiants (corrigés au préalable) donnée, on recrée la bonne String (isbn ou ean) pour créer l'objet Document plus tard.
	 * @param listIsbnOuEan Liste d'identifiant, isbn ou ean en fonction de l'appel.
	 * @return Une String reformée à partir de la liste.
	 */
	public static String reformerIsbnOuEan(ArrayList<String> listIsbnOuEan) {
		String out;
		if (listIsbnOuEan.size() == 1) out = listIsbnOuEan.get(0); // S'il n'y a qu'un seul ISBN
		else if (listIsbnOuEan.size() > 1) {
			out = "";
			for (String item : listIsbnOuEan) {
				out += item;
				// Si l'item n'est pas le dernier
				if (!item.equals(listIsbnOuEan.get(listIsbnOuEan.size() - 1))) out += ", ";
			}
		} else out = "";
		return out;
	}

	/**
	 * Vérifie si l'identifiant de document est un ISBN (donc un livre) ou sinon un EAN.
	 * @param idDocument L'identifiant que l'on souhaite tester.
	 * @return Si le document contient un -, c'est un ISBN, car lui en contient et non pas l'EAN.
	 */
	public static boolean estISBN(String idDocument) { return idDocument.contains("-"); }
}
