package application.oeuvres.types;

import application.oeuvres.Document;

/**
 * Classe héritant de Document et représentant un CD.
 */
public class CD extends Document {
    /**
     * @param ean Numéro d'EAN unique identifiant le document. @see <a href="https://fr.wikipedia.org/wiki/EAN_13">Page Wikipedia sur l'ean.</a>
     * @param title Titre du document.
     * @param publisher Publicateur du document.
     * @param date Date de publication.
     * @param authorName Nom de l'autheur.
     * @param authorSurname Prénom de l'autheur.
     * @param totalCopies Nombre total de copies disponibles réparties entre les différentes bibliothèques de paris.
     * @see application.oeuvres.Document#Document(String, String, String, String, String, String, int)
     */
    public CD(String ean, String title, String publisher, String date, String authorName, String authorSurname, int totalCopies) {
        super(ean, title, publisher, date, authorName, authorSurname, totalCopies);
    }
}
