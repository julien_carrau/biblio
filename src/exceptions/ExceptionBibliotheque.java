package exceptions;

/**
 * Exception concernant la création et la gestion de bibliothèques.
 */
public class ExceptionBibliotheque extends Exception {
    public ExceptionBibliotheque(String message) {
        super(message);
    }
}
