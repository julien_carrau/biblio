package application.acteurs;

import application.oeuvres.Document;
import application.oeuvres.livres.Livre;
import data.FileReader;
import exceptions.ExceptionEmpruntRemise;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Classe modélisant une bibliothèque, référençant l'ensemble de ses documents et utilisateurs.
 */
public class Bibliotheque {
    private final String name;
    private HashMap<String, Integer> quantiteLivres, quantiteDocuments;
    private HashMap<Integer, Utilisateur> utilisateurs;

    /**
     * Constructeur de la classe.
     * @param name Le nom de la nouvelle bibliothèque.
     */
    public Bibliotheque(String name) {
        this.name = name;
        this.quantiteLivres = new HashMap<>();
        this.quantiteDocuments = new HashMap<>();
        this.utilisateurs = new HashMap<>();
    }

    /**
     * Méthode d'accès au nom de la bibliothèque.
     * @return Le nom de la bibliothèque.
     */
    public String getName() { return name; }

    /**
     * Retourne la quantité de documents présente dans cette bibliothèque correspondant à l'identifiant donné.
     * @param idDocument EAN ou ISBN identifiant le document voulu.
     * @return La quantité sauvegardée, -1 si le document n'est pas enregistré dans cette bibliothèque.
     */
    public int getQuantite(String idDocument) {
        Integer result;
        if (FileReader.estISBN(idDocument))
            result = quantiteLivres.get(idDocument);
        else
            result = quantiteDocuments.get(idDocument);
        if (result == null) return -1;
        return result;
    }

    /**
     * Méthode d'accès à un utilisateur par son identifiant.
     * @param idUtilisateur L'identifiant de l'utilisateur.
     * @return Cet utilisateur.
     */
    public Utilisateur getUtilisateur(Integer idUtilisateur) { return utilisateurs.get(idUtilisateur); }

    /**
     * Ajoute un document dans les stocks de la bibliothèque.
     * Attention : On ne peut ajouter qu'un exemplaire à la fois.
     * @param idDocument EAN ou ISBN identifiant le document à enregistrer.
     */
    public void ajouterDocument(String idDocument) {
        if (FileReader.estISBN(idDocument)) {
            if (quantiteLivres.containsKey(idDocument)) quantiteLivres.replace(idDocument, quantiteLivres.get(idDocument) + 1);
            else quantiteLivres.put(idDocument, 1);
        } else {
            if (quantiteDocuments.containsKey(idDocument)) quantiteDocuments.replace(idDocument, quantiteDocuments.get(idDocument) + 1);
            else quantiteDocuments.put(idDocument, 1);
        }
    }

    /**
     * Ajoute un utilisateur à la bibliothèque.
     * @param utilisateur un utilisateur pourvu d'un identifiant, d'un quota, et d'une bibliothèque associée.
     */
    public void ajouterUtilisateur(Utilisateur utilisateur) {
        this.utilisateurs.put(utilisateur.getId(), utilisateur);
    }

    /**
     * Récupère la totalité des identifiants enregistrés dans cette bibliothèque, aussi bien enregistrés par ISBN ou EAN.
     * @return Une liste contenant tous les identifiants connus par la bibliothèque (ISBN et EAN confondus).
     */
    public List<String> recupererTousLesIdentifiants() {
        List<String> out = new ArrayList<>(quantiteDocuments.keySet());
        out.addAll(quantiteLivres.keySet());
        return out;
    }

    /**
     * Permet à un utilisateur d'emprunter un document dans la bibliothèque.
     * @param doc Document voulu.
     * @param idUtilisateur Identifiant de l'utilisateur effectuant l'emprunt.
     * @throws ExceptionEmpruntRemise Si le document n'existe pas, si l'identifiant de l'utilisateur est incorrect, s'il n'y a plus d'exemplaires dispos, si l'utilisateur a dépassé son quota d'emprunts.
     */
    protected void preterDocumentAUtili(Document doc, Integer idUtilisateur) throws ExceptionEmpruntRemise {
        if (!(quantiteDocuments.containsKey(doc.getEan()) || (doc instanceof Livre && quantiteLivres.containsKey(((Livre) doc).getIsbn()))))
            throw new ExceptionEmpruntRemise("Le document souhaité pour l'emprunt n'existe pas dans cette bibliothèque.");

        if (!utilisateurs.containsKey(idUtilisateur))
            throw new ExceptionEmpruntRemise("L'identifiant d'utilisateur est incorrect.");

        if (getQuantite(doc.getEan()) == 0 || (doc instanceof Livre && getQuantite(((Livre) doc).getIsbn()) == 0))
            throw new ExceptionEmpruntRemise("Il n'y a plus d'exemplaire disponible de ce document dans cette bibliothèque.");

        if (getUtilisateur(idUtilisateur).getNombreDocumentsEmpruntes() == getUtilisateur(idUtilisateur).getQuotaMaxDocuments())
            throw new ExceptionEmpruntRemise("L'utilisateur a dépassé son quota de documents empruntés.");

        // On actualise les documents de la bibliothèque
        if (doc instanceof Livre && quantiteLivres.containsKey(((Livre) doc).getIsbn()))
            quantiteLivres.replace(((Livre) doc).getIsbn(), quantiteLivres.get(((Livre) doc).getIsbn()) - 1);
        if (quantiteDocuments.containsKey(doc.getEan()))
            quantiteDocuments.replace(doc.getEan(), quantiteDocuments.get(doc.getEan()) - 1);
        // On ajoute le document à l'utilisateur
        getUtilisateur(idUtilisateur).emprunter(doc);
    }

    /**
     * Permet à un utilisateur de rendre un document à la bibliothèque.
     * @param doc Document voulu.
     * @param idUtilisateur Identifiant de l'utilisateur effectuant l'emprunt.
     * @throws ExceptionEmpruntRemise Si l'identifiant d'utilisateur est incorrect, si le document n'est pas dans la liste des documents empruntés par l'utilisateur.
     */
    protected void rendreDocument(Document doc, Integer idUtilisateur) throws ExceptionEmpruntRemise {
        if (!utilisateurs.containsKey(idUtilisateur))
            throw new ExceptionEmpruntRemise("L'identifiant d'utilisateur est incorrect.");

        Utilisateur u = getUtilisateur(idUtilisateur);

        if (!u.getDocumentsEmpruntes().contains(doc))
            throw new ExceptionEmpruntRemise("Le document n'est pas dans la liste des documents empruntés par l'utilisateur.");

        // On actualise les documents de la bibliothèque
        if (doc instanceof Livre && quantiteLivres.containsKey(((Livre) doc).getIsbn()))
            quantiteLivres.replace(((Livre) doc).getIsbn(), quantiteLivres.get(((Livre) doc).getIsbn()) + 1);
        if (quantiteDocuments.containsKey(doc.getEan()))
            quantiteDocuments.replace(doc.getEan(), quantiteDocuments.get(doc.getEan()) + 1);
        // On enlève le document à l'utilisateur
        u.rendre(doc);
    }

    /**
     * Permet à une bibliothèque de donner un document à une autre bibliothèque.
     * @param doc Document voulu.
     * @param biblio La bibliothèque emprunteuse.
     * @throws ExceptionEmpruntRemise Si le document n'existe pas ou s'il n'y a plus d'exemplaires dispos.
     */
    protected void envoyerDocumentABiblio(Document doc, Bibliotheque biblio) throws ExceptionEmpruntRemise {
        if (!(quantiteDocuments.containsKey(doc.getEan()) || (doc instanceof Livre && quantiteLivres.containsKey(((Livre) doc).getIsbn()))))
            throw new ExceptionEmpruntRemise("Le document souhaité pour le don n'existe pas dans cette bibliothèque.");

        if (getQuantite(doc.getEan()) == 0 || (doc instanceof Livre && getQuantite(((Livre) doc).getIsbn()) == 0))
            throw new ExceptionEmpruntRemise("Il n'y a plus d'exemplaire disponible de ce document dans cette bibliothèque.");

        // On actualise les documents de la bibliothèque
        if (doc instanceof Livre && quantiteLivres.containsKey(((Livre) doc).getIsbn())) {
            // On décrémente la qté de la biblio prêteuse, ou on la supprime si == 0
            if (quantiteLivres.get(((Livre)doc).getIsbn()) == 1)
                quantiteLivres.remove(((Livre)doc).getIsbn());
            else quantiteLivres.replace(((Livre) doc).getIsbn(), quantiteLivres.get(((Livre) doc).getIsbn()) - 1);
            // On incrémente la qté de la biblio emprunteuse ou on crée la clé si absente
            biblio.quantiteLivres.merge(((Livre) doc).getIsbn(), 1, Integer::sum);
        }
        if (quantiteDocuments.containsKey(doc.getEan())) {
            // On décrémente la qté de la biblio prêteuse, ou on la supprime si == 0
            if (quantiteDocuments.get(doc.getEan()) == 1)
                quantiteDocuments.remove(doc.getEan());
            else quantiteDocuments.replace(doc.getEan(), quantiteDocuments.get(doc.getEan()) - 1);
            // On incrémente la qté de la biblio emprunteuse ou on crée la clé si absente
            biblio.quantiteDocuments.merge(doc.getEan(), 1, Integer::sum);
        }

        System.out.println("Document envoyé de la bibliothèque " + getName() + " à la bibliothèque " + biblio.getName() + " avec succès !");
    }

    /**
     * Renvoie une représentation textuelle d'une bibliothèque contenant le nom,
     * les quantités de livres, de documents et d'utilisateurs de la bibliothèque.
     * @return Cette représentation textuelle.
     */
    @Override
    public String toString() {
        return "Bibliothèque : " + getName() + "\n" +
                "Nombre de livres : " + quantiteLivres.size() + "\n" +
                "Nombre de documents : " + quantiteDocuments.size() + "\n" +
                "Nombre d'utilisateurs : " + utilisateurs.size() + "\n";
    }
}
