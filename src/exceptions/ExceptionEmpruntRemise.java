package exceptions;

/**
 * Exception concernant l'emprunt et la remise de documents entre acteurs du réseau.
 */
public class ExceptionEmpruntRemise extends Exception {
    public ExceptionEmpruntRemise(String message) {
        super(message);
    }
}