# Projet Java

### But
> Application de gestion des documents d'une bibliothèque. Les données sont chargées à partir du site https://opendata.paris.fr (format CSV).

### Éléments
> - Documents (Bande Dessinée, Carte, CD, Vinyle, Jeu de société, Jeu vidéo, Livre, Partition, Revue, Autre)
> - Bibliothèque
> - Réseau de bibliothèques
> - Utilisateurs

### Exécution
> En partant de la racine : `java -jar Biblio.jar src/data/data.csv`

### Fonctionnalités, suivies de comment les appeler dans l'interface console
> 1) Ajouter une nouvelle bibliothèque dans le réseau. 1 nomBibliothèque
> 2) Ajouter un nouveau document dans le réseau par ISBN ou EAN (détection automatique). 2 identifiantDocument 
> 3) Ajouter un nouvel utilisateur, associé à une bibliothèque, avec un quota maximum de documents qu’il peut emprunter. 3 nomBibliothèque quotaMaxDocumentsEmpruntables
> 4) Consulter l’ensemble des documents. 4 nomBibliothèque(optionnel)
> 5) Consulter pour une série, les documents triés par date de publication. 5 nomDeSérie nomBibliothèque(opt.)
> 6) Consulter les documents d’un même auteur (par nom, prénom ou les deux). 6 nomAuteur prénomAuteur nomBibliothèque(opt.)
> 7) Rechercher un livre par son ISBN. 7 isbn nomBibliothèque(opt.)
> 8) Recherche un document par son EAN. 8 ean nomBibliothèque(opt.)
> 9) Consulter le nombre de documents de chaque type publiés de annéeDébut à annéeFin (0 à aujourd'hui par défaut). 9 annéeDébut(opt.) annéeFin(opt.) nomBibliothèque(opt.)
> 10) L'utilisateur identifié emprunte le document souhaité dans sa bibliothèque. 10 idDocument idUtilisateur
> 11) L'utilisateur identifié rend le document préalablement emprunté dans sa bibliothèque. 11 idDocument idUtilisateur
> 12) La première bibliothèque reçoit le document spécifié de la deuxième. 12 idDocument nomBiblioReceveuse nomBiblioDonneuse
>
> Pour les actions 4 à 9, pour restreindre la recherche à une bibliothèque en particulier, ajouter le nom de la bibliothèque en dernier paramètre.
> Exemple : 6 Jean Racine OscarWilde : consulter les documents publiés par Jean Racine dans la bibliothèque Oscar Wilde.