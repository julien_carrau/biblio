package exceptions;

/**
 * Exception concernant la gestion de documents.
 */
public class ExceptionDocument extends Exception {
    public ExceptionDocument(String message) {
        super(message);
    }
}
