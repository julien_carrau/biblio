package application.oeuvres;

/**
 * Classe abstraite représentant un document identifié par son ean.
 * Tous les documents sont voués à être stockés dans le réseau de bibliothèques.
 */
public abstract class Document {
    private String ean;
    private String title;
    private String publisher;
    private String date;
    private String authorSurname, authorName;
    private int totalCopies;

    /**
     * Constructeur de la classe.
     * @param ean Numéro d'EAN unique identifiant le document. @see <a href="https://fr.wikipedia.org/wiki/EAN_13">Page Wikipedia sur l'ean.</a>
     * @param title Titre du document.
     * @param publisher Publicateur du document.
     * @param date Date de publication.
     * @param authorName Nom de l'autheur.
     * @param authorSurname Prénom de l'autheur.
     * @param totalCopies Nombre total de copies disponibles réparties entre les différentes bibliothèques de paris.
     */
    public Document(String ean, String title, String publisher, String date, String authorName, String authorSurname, int totalCopies) {
        this.ean = ean;
        this.title = title;
        this.publisher = publisher;
        this.date = date;
        this.authorSurname = authorSurname;
        this.authorName = authorName;
        this.totalCopies = totalCopies;
    }

    /**
     * Méthode d'accès à l'EAN du document.
     * @return L'EAN du document.
     */
    public String getEan() { return ean; }

    /**
     * Méthode d'accès au titre du document.
     * @return Le titre du document.
     */
    public String getTitle() { return title; }

    /**
     * Méthode d'accès à l'éditeur du document.
     * @return L'éditeur du document.
     */
    public String getPublisher() { return publisher; }

    /**
     * Méthode d'accès à la date de publication du document.
     * @return La date de publication du document.
     */
    public String getDate() { return date; }

    /**
     * Méthode d'accès au prénom de l'auteur du document.
     * @return Le prénom de l'auteur du document.
     */
    public String getAuthorSurname() { return authorSurname; }

    /**
     * Méthode d'accès au nom de l'auteur du document.
     * @return Le nom de l'auteur du document.
     */
    public String getAuthorName() { return authorName; }

    /**
     * Méthode d'accès au nombre de copies du document dans le réseau.
     * @return Le nombre de copies du document dans le réseau.
     */
    public int getTotalCopies() { return totalCopies; }

    /**
     * Renvoie une représentation textuelle d'un document,
     * contenant tous ses attributs.
     * @return Cette représentation textuelle.
     */
    @Override
    public String toString() {
        return "EAN : " + getEan() + "\n" +
                "Title : " + getTitle() + "\n" +
                "Publisher : " + getPublisher() + "\n" +
                "Date de parution : " + getDate() + "\n" +
                "Author surname : " + getAuthorSurname() + "\n" +
                "Author name : " + getAuthorName() + "\n" +
                "Total copies : " + getTotalCopies() + "\n";
    }
}
